
//--------------------------------------------------------------
// Title:
// ------
// 2. Horizontal-bar-chart
// 
// Description:
// ------------
//The aim of this exercise is to create a horizontal bar chart
// using the variable 'data' that is an array with seven values
//
// What's your task?
// -----------------
// Based on the exercise 1. Simple-bar-chart please modify it to 
// create a horizontal bar chart
//
// Evaluation
// ----------
// The resulting output should be similar to the image that you
// you can find in '2.Horizontal-bar-chart/evaluation.png'
// 
//--------------------------------------------------------------

function setup() {

  // The data to visualize
  var data = [105, 212, 158, 31, 98, 54],
      maxData = max(data);
  
  // Canvas dimension (width and height)
  var width = 200, 
      height = 300;
  createCanvas(width, height);
  
  // Defining the bars of our graphic 
    // We define two variables here (barHeight and barMargin).
    // barMargin is the space between two bars
  var barHeight =  (height / data.length) * .9, 
      barMargin = (height / data.length) * .1;
  
  // We go over all data points
  for(var i=0; i<data.length; i++) {
    
    push();
    // We jump to the bottom left corner of the bar
    translate(0, i * (barHeight + barMargin)); 
    // We draw the bar
    fill('steelblue'); noStroke();
    var barWidth = map(data[i], 0, maxData,  0, width)
    rect(0, 0, barWidth, barHeight);
    // We draw the value
    fill('#FAFAFA');
    text(data[i], width * .025, barHeight/2 );
    pop();
  }
}