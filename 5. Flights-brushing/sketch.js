
//--------------------------------------------------------------
// Title:
// ------
// 5.Flights-brushing
// 
// Description:
// ------------
//The aim of this exercise is visualize departure coordinates of 
// flights dataset and implement the brushing interaction to highlight
// brushed elements.
//  
//
// What's your task?
// -----------------
// Based on the exercise the set of exercise performed last week, complete
// the following code.
//
// Evaluation
// ----------
// The resulting output should be similar to the image that you
// you can find in '5.Flights-brushing/evaluation.png'
// 
//--------------------------------------------------------------



var table, rows;

// Start position for brushing interaction
var startSelectX, startSelectY;
// Off-screen graphics buffer to save all the airports
var airportsImage;

var flight = function(row){
  this.distance = row.getNum("distance");
  this.from_long = row.getNum("from_long");
  this.from_lat = row.getNum("from_lat");
  this.departureX = map(this.from_long,-180,180,0,width);
  this.departureY = map(this.from_lat,-90,90,height,0);
}


function preload() {
  table = loadTable("flights.csv","csv","header");
  rows = table.getRows();
}

function setup() {
  createCanvas(900,400);
  noLoop();
  
  // Drawing all airports and saving them into airportsImage variable
  airportsImage = createGraphics(width,height);
  for (var r = 0; r < rows.length; r++) {
    var thisFlight = new flight(rows[r]);
    airportsImage.noStroke();
    airportsImage.fill(160,155);
    airportsImage.ellipse(thisFlight.departureX,thisFlight.departureY, 3, 3);
  }
  
}

function draw() {
  image(airportsImage, 0, 0);
}

// Mouse interaction
function mousePressed() {
  startSelectX = mouseX;
  startSelectY = mouseY;
}

function mouseDragged() {
  clear()
  redraw();
  fill(0,0,0,50);
  rect(startSelectX, startSelectY, mouseX-startSelectX, mouseY-startSelectY);
}

function mouseReleased() {
  clear();
  redraw();
  
  stopSelectX = mouseX;
  stopSelectY = mouseY;

  startX = min(startSelectX, stopSelectX);
  stopX =  max(startSelectX, stopSelectX);
  startY = min(startSelectY, stopSelectY);
  stopY =  max(startSelectY, stopSelectY);

  selectDataPoints(startX, startY, stopX, stopY);
}


function selectDataPoints(startX, startY, stopX, stopY) {
  for (var r = 0; r < rows.length; r++) {
    var thisFlight = new flight(rows[r]);
    if(startX < thisFlight.departureX && thisFlight.departureX < stopX &&
      startY < thisFlight.departureY  && thisFlight.departureY  < stopY ) {
      noStroke()
      fill(255,0,0);
      ellipse(thisFlight.departureX, thisFlight.departureY,3,3);
    }
  }
}