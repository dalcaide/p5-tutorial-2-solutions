
//--------------------------------------------------------------
// Title:
// ------
// 3. Function-bar-chart
// 
// Description:
// ------------
// The aim of this exercise is to create a horizontal bar chart using 
// the variable 'data' that is a javascript object that has seven elements. 
// Each element is composed of a string (name) and a number (value).
//
// What's your task?
// -----------------
// Based on the exercise 2. Horizontal-bar-chart please modify it to 
// complete the code
//
// Evaluation
// ----------
// The resulting output should be similar to the image that you
// you can find in '3.Function-bar-chart/evaluation.png'
// 
//--------------------------------------------------------------

// The data to visualize
var data = [{'name':'France',  'value': 212}, 
            {'name':'Spain',   'value': 105},
            {'name':'Belgium', 'value': 54},
            {'name':'Germany', 'value': 158},
            {'name': 'Italy',  'value': 98},
            {'name':'Grece',   'value': 31}];


function setup() {
  // Canvas dimension (width and height)
  var width = 400, 
      height = 300;
  createCanvas(width, height);
  noLoop();
}

function draw () {
    barchart(data);
}


function barchart(data) {
  
  // We define the space for the label of our data
  var margin = width * 0.2;
  // We compute the max value of our data
  var maxData = maxValueFromData(data);
  
    // Defining the bars of our graphic 
    // We define two variables here (barHeight and barMargin).
    // barMargin is the space between two bars
  var barHeight =  (height / data.length) * .9, 
      barMargin = (height / data.length) * .1;

  // We go over all data points
  for(var i=0; i<data.length; i++) {
    
    push();
    translate(0, i * (barHeight + barMargin));
    fill(0);
    text(data[i].name, margin * .25, barHeight/2 );
    pop();
    
    push();
    // We jump to the bottom left corner of the bar
    translate(margin, i * (barHeight + barMargin)); 
    // We draw the bar
    fill('steelblue'); noStroke();
    var barWidth = map(data[i].value, 0, maxData,  margin, width)
    rect(0, 0, barWidth, barHeight);
    // We draw the value
    fill('#FAFAFA');
    text(data[i].value, width * .025, barHeight/2 );
    pop();
  }
}


function maxValueFromData(data){
  values = [];
  data.forEach(function(d){ values.push(d.value) });
  return max(values);
}

