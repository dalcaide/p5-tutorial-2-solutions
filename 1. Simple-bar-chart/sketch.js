
//--------------------------------------------------------------
// Title:
// ------
// 1. Simple-bar-chart
// 
// Description:
// ------------
//The aim of this exercise is to create a bar chart
// using the variable 'data' that is an array with seven values
//
// What's your task?
// -----------------
// This code is incomplete, the parts that need to be completed 
// are indicated by the word 'TODO'
//
// Evaluation
// ----------
// The resulting output should be similar to the image that
// you can find in '1.Bar-chart/evaluation.png'
// 
//--------------------------------------------------------------

function setup() {

  // The data to visualize
  var data = [105, 212, 158, 31, 98, 54],
      maxData = max(data);
  
  // Canvas dimension (width and height)
  var width = 500, 
      height = 200;
  createCanvas(width, height);
  
  // Defining the bars of our graphic 
    // We define two variables here (barWidth and barMargin).
    // barMargin is the space between two bars
  var barWidth =  (width / data.length) * .9, 
      barMargin = (width / data.length) * .1;
  
  // We go over all data points
  for(var i=0; i<data.length; i++) {
    
    push(); // Read 'NOTE 1' below
    // We jump to the bottom left corner of the bar
    translate(i * (barWidth + barMargin), 0); 
    // We draw the bar
    fill('steelblue'); noStroke();
    var barHeight = map(data[i], 0, maxData,  0, -height)
    rect(0, height, barWidth, barHeight); // Read 'NOTE 2' below
    // We draw the value
    fill('#FAFAFA');
    text(data[i], barWidth/2, height * (1-.025) );
    pop();
  }
}

//--------------------------------------------------------------
// NOTE 1: The push() function saves the current drawing style 
// settings and transformations, while pop() restores these settings. 
// Note that these functions are always used together. 
// They allow you to change the style and transformation settings 
// and later return to what you had. 
// REFERENCE: https://p5js.org/reference/#/p5/pop
//
// NOTE 2: The coordinate (0,0) is the TOP-LEFT of the screen.
// This is different from what you might be  used to 
// (i.e. where origin is BOTTOM-LEFT of screen). 
//--------------------------------------------------------------