
//--------------------------------------------------------------
// Title:
// ------
// 6.Map-and-barchart
// 
// Description:
// ------------
//The aim of this exercise to combine the exercise 4. and 5. to create
// an interactive map that use the brushing interaction to select that for 
// the barchart
//  
// What's your task?
// -----------------
// Based on the last two exercises comete the following code.
//
// Evaluation
// ----------
// The resulting output should be similar to the image that you
// you can find in '6.Map-and-barchart/evaluation.png'
// 
//--------------------------------------------------------------

var table, rows;
var airports;
var selectedFlights = [];
var startSelectX, startSelectY;

var heightCanvas = 400;
var widthCanvas =  1200;
var widthMap = widthCanvas * .8;
var widthBar = widthCanvas * .4;

var flight = function(row){
  this.distance = row.getNum("distance");
  this.from_long = row.getNum("from_long");
  this.from_lat = row.getNum("from_lat");
  this.departureX = map(this.from_long,-180,180,0,widthMap);
  this.departureY = map(this.from_lat,-90,90, heightCanvas,0);
}

function preload() {
  table = loadTable("flights.csv","csv","header");
  rows = table.getRows();
}

function setup() {
  createCanvas(widthCanvas, heightCanvas);
  noLoop();
  
  airportsImage = createGraphics(widthCanvas, heightCanvas);
  for (var r = 0; r < rows.length; r++) {
    var thisFlight = new flight(rows[r]);
    airportsImage.noStroke();
    airportsImage.fill(160,160,160,160);
    airportsImage.ellipse(thisFlight.departureX,thisFlight.departureY, 3, 3);
  }
}

function draw() {
  image(airportsImage, 0, 0);
}

function mousePressed() {
  startSelectX = mouseX;
  startSelectY = mouseY;
}

function mouseDragged() {
  clear()
  redraw();
  fill(0,0,0,50);
  rect(startSelectX, startSelectY, mouseX-startSelectX, mouseY-startSelectY);
}

function mouseReleased() {
  clear();
  redraw();
  
  stopSelectX = mouseX;
  stopSelectY = mouseY;

  startX = min(startSelectX, stopSelectX);
  stopX =  max(startSelectX, stopSelectX);
  startY = min(startSelectY, stopSelectY);
  stopY =  max(startSelectY, stopSelectY);

  selectDataPoints(startX, startY, stopX, stopY);
  drawBarchart();
}

function selectDataPoints(startX, startY, stopX, stopY) {
  selectedFlights = [];
  
  for (var r = 0; r < rows.length; r++) {
    var thisFlight = new flight(rows[r]);
    if(startX < thisFlight.departureX && thisFlight.departureX < stopX &&
      startY < thisFlight.departureY  && thisFlight.departureY  < stopY ) {
      noStroke()
      fill(255,0,0);
      ellipse(thisFlight.departureX, thisFlight.departureY,3,3);
      selectedFlights.push(thisFlight.distance);
    }
  }
}

function drawBarchart(){
  data = countQuantiles(selectedFlights);
  push();
  translate(widthMap,0)
  barchart(data);
  pop();
}


function barchart(data) {
  // 
  var margin = widthBar * 0.2;
  
    // Defining the bars of our graphic 
    // We define two variables here (barHeight and barMargin).
    // barMargin is the space between two bars
  var barHeight =  (heightCanvas / data.length) * .9, 
      barMargin = (heightCanvas / data.length) * .1;

  // We go over all data points
  for(var i=0; i<data.length; i++) {
    
    push();
    translate(0, i * (barHeight + barMargin));
    fill(0);
    text(data[i].name, margin * .25, barHeight/2 );
    pop();
    
    push();
    // We jump to the bottom left corner of the bar
    translate(margin, i * (barHeight + barMargin)); 
    // We draw the bar
    fill('steelblue'); noStroke();
    var barWidth = map(data[i].value, 0, 100,  0, widthBar - margin)
    rect(0, 0, barWidth, barHeight);
    // We draw the value
    fill(0);
    text(data[i].value + " %", (widthBar - margin) * .25, barHeight/2 );
    pop();
  }
}

function countQuantiles (arr) {
  var quantiles = [
    {"name":"[0:299)", "min":0, "max": 299, 'value': 0},
    {"name":"[299:473)", "min":299, "max": 473, 'value': 0},
    {"name":"[473:666)", "min":473, "max": 666, 'value': 0},
    {"name":"[666:875)", "min":666, "max": 875, 'value': 0},
    {"name":"[875:1131)", "min":875, "max": 1131, 'value': 0}, 
    {"name":"[1131:1445)", "min":1131, "max": 1445, 'value': 0},  
    {"name":"[1445:1870)", "min":1445, "max": 1870, 'value': 0}, 
    {"name":"[1870:2669)", "min":1870, "max": 2669, 'value': 0},
    {"name":"[2669:4397)", "min":2669, "max": 4397, 'value': 0}, 
    {"name":"[>4397)", "min":4397, "max": 99999999, 'value': 0}
  ]
  
    arr.sort();
    for (var j = 0; j < quantiles.length; j++) {
      for ( var i = 0; i < arr.length; i++ ) {
        if ( quantiles[j].min < +arr[i] && +arr[i] < quantiles[j].max) {
          quantiles[j].value++;
        }
      }
    }
    
    for (j = 0; j < quantiles.length; j++) {
      quantiles[j].value = Math.round(1000 * quantiles[j].value / arr.length) / 10;
    }
    return quantiles;
}
