
//--------------------------------------------------------------
// Title:
// ------
// 4.Function-reading-flights-data
// 
// Description:
// ------------
//The aim of this exercise is to create a horizontal bar chart
// reading the file flights.csv
//
// What's your task?
// -----------------
// Based on the exercise 3. Function-bar-chart please modify it to 
// complete the code
//
// Evaluation
// ----------
// The resulting output should be similar to the image that you
// you can find in '4.Function-reading-flights-data/evaluation.png'
// 
//--------------------------------------------------------------

var table;

function preload() {
  table = loadTable("flights.csv","csv","header");
}

function setup() {
  // Canvas dimension (width and height)
  var width = 500, 
      height = 300;
  createCanvas(width, height);
  noLoop();
  data = countQuantiles(table.getColumn('distance'))
}


function draw () {
    barchart(data);
}


function barchart(data) {
  // We define the space for the label of our data
  var margin = width * 0.2;
  
    // Defining the bars of our graphic 
    // We define two variables here (barHeight and barMargin).
    // barMargin is the space between two bars
  var barHeight =  (height / data.length) * .9, 
      barMargin = (height / data.length) * .1;

  // We go over all data points
  for(var i=0; i<data.length; i++) {
    
    push();
    translate(0, i * (barHeight + barMargin));
    fill(0);
    text(data[i].name, margin * .25, barHeight/2 );
    pop();
    
    push();
    // We jump to the bottom left corner of the bar
    translate(margin, i * (barHeight + barMargin)); 
    // We draw the bar
    fill('steelblue'); noStroke();
    var barWidth = map(data[i].value, 0, 100, 0 , width - margin)
    rect(0, 0, barWidth, barHeight);
    // We draw the value
    fill(0);
    text(data[i].value + " %", (width-margin) * .25, barHeight/2 );
    pop();
  }
}


function countQuantiles (arr) {
  var quantiles = [
    {"name":"[0:299)", "min":0, "max": 299, 'value': 0},
    {"name":"[299:473)", "min":299, "max": 473, 'value': 0},
    {"name":"[473:666)", "min":473, "max": 666, 'value': 0},
    {"name":"[666:875)", "min":666, "max": 875, 'value': 0},
    {"name":"[875:1131)", "min":875, "max": 1131, 'value': 0}, 
    {"name":"[1131:1445)", "min":1131, "max": 1445, 'value': 0},  
    {"name":"[1445:1870)", "min":1445, "max": 1870, 'value': 0}, 
    {"name":"[1870:2669)", "min":1870, "max": 2669, 'value': 0},
    {"name":"[2669:4397)", "min":2669, "max": 4397, 'value': 0}, 
    {"name":"[>4397)", "min":4397, "max": 99999999, 'value': 0}
  ]
  
    arr.sort();
    for (var j = 0; j < quantiles.length; j++) {
      for ( var i = 0; i < arr.length; i++ ) {
        if ( quantiles[j].min < +arr[i] && +arr[i] < quantiles[j].max) {
          quantiles[j].value++;
        }
      }
    }
    
    for (j = 0; j < quantiles.length; j++) {
      quantiles[j].value = Math.round(1000 * quantiles[j].value / arr.length) / 10;
    }
    return quantiles;
}

